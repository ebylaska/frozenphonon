#include	<stdlib.h>
#include	<stdio.h>
#include	<string.h>
#include	<math.h>
#include	"blas.h"




/***********************************************
 *                                             *
 *               ascii_plotter                 *
 *                                             *
 ***********************************************/
#define maxrow_ascii  30
#define shift1_ascii  2
#define shift2_ascii  11
#define maxcol_ascii  86
static double xmin_ascii=0.0;
static double ymin_ascii=0.0;
static double xmax_ascii=1.0;
static double ymax_ascii=1.0;

static int ascii_xscale(double x)
{
   return ( (int) (round((x-xmin_ascii)*(maxcol_ascii-1.0)/(xmax_ascii-xmin_ascii))));
}

static int ascii_yscale(double y)
{
   return ((int) round((y-ymax_ascii)*(maxrow_ascii-1.0)/(ymin_ascii-ymax_ascii)) );
}

void  ascii_plotter(FILE *fp, int n, int m,  double *x, double *y, char *symbols,
                   char *title, char *xlabel, char *ylabel,
                   double xmin,double xmax, double ymin, double ymax, int plotzero)
{
   int shift_ascii,i,j,jj,u,v,va,vb,yzero;
   char nstru[15],nstrd[15],s;
   char point[maxrow_ascii][maxcol_ascii+shift1_ascii+shift2_ascii];

   xmin_ascii = xmin;
   ymin_ascii = ymin;
   xmax_ascii = xmax;
   ymax_ascii = ymax;
   shift_ascii = shift1_ascii+shift2_ascii;

   for (j=0; j<(maxcol_ascii+shift_ascii); ++j)
   for (i=0; i<maxrow_ascii; ++i)
      point[i][j] = ' ';


   /* set y-axis */
   for (j=0; j<maxrow_ascii; ++j)
      point[j][shift1_ascii+10] = ':';


   if ((ascii_xscale(0.0)<(maxcol_ascii-1)) && (ascii_xscale(0.0)>0))
      for (j=0; j<maxrow_ascii; ++j)
         point[j][shift_ascii-1+ascii_xscale(0.00)] = '.';


   /* set x-axis */
   for (i=shift_ascii; i<(maxcol_ascii+shift_ascii); ++i)
      point[ascii_yscale(0.0)][i] = '.';


   /* set ylabels */
   sprintf(nstru,"%10.3e",ymax);
   sprintf(nstrd,"%10.3e",ymin);
   for (i=0; i<10; ++i)
   {
      point[0][shift1_ascii+i] = nstru[i];
      point[maxrow_ascii-1][shift1_ascii+i] = nstrd[i];
   }
   if ((ascii_yscale(0.0)<(maxrow_ascii-1)) && (ascii_yscale(0.0)>0))
   {
      point[ascii_yscale(0.00)][shift1_ascii+0]  = ' ';
      point[ascii_yscale(0.00)][shift1_ascii+1]  = '0';
      point[ascii_yscale(0.00)][shift1_ascii+2]  = '.';
      point[ascii_yscale(0.00)][shift1_ascii+3]  = '0';
      point[ascii_yscale(0.00)][shift1_ascii+4]  = '0';
      point[ascii_yscale(0.00)][shift1_ascii+5]  = '0';
      point[ascii_yscale(0.00)][shift1_ascii+6]  = 'e';
      point[ascii_yscale(0.00)][shift1_ascii+7]  = '+';
      point[ascii_yscale(0.00)][shift1_ascii+8]  = '0';
      point[ascii_yscale(0.00)][shift1_ascii+9]  = '0';
   }

   /* set ylabel */
   jj = (maxrow_ascii-strlen(ylabel))/2;
   for (j=0; j<strlen(ylabel); ++j)
       point[jj+j][0] = ylabel[j];


   /* plot points */
   if (plotzero)
   {
      for (j=0; j<m; ++j)
      {
         s = symbols[j];
         for (i=0; i<n; ++i)
         {
            u = ascii_xscale(x[i]);
            v = ascii_yscale(y[i+j*n]);
            point[v][shift_ascii+u] = s;

         }
      }
   }
   else
   {
      yzero = ascii_yscale(0.0);
      for (j=0; j<m; ++j)
      {
         s  = symbols[j];
         u  = ascii_xscale(x[0]);
         v  = ascii_yscale(y[j*n+0]);
         va = ascii_yscale(y[j*n+1]);
         if ((v!=yzero) && (va!=yzero)) point[v][shift_ascii+u] = s;
         for (i=1; i<(n-1); ++i)
         {
            u  = ascii_xscale(x[i]);
            vb = ascii_yscale(y[j*n+i-1]);
            v  = ascii_yscale(y[j*n+i]);
            va = ascii_yscale(y[j*n+i+1]);
            if ((v!=yzero) && (vb!=yzero) && (va!=yzero)) point[v][shift_ascii+u] = s;
         }
         u  = ascii_xscale(x[n-1]);
         vb = ascii_yscale(y[j*n+n-2]);
         v  = ascii_yscale(y[j*n+n-1]);
         if ((v!=yzero) && (vb!=yzero)) point[v][shift_ascii+u] = s;
      }
   }


   fprintf(fp,"\n");
   for (i=0; i<(shift_ascii+(maxcol_ascii-strlen(title))/2); ++i) fprintf(fp," ");
   fprintf(fp,"%s\n\n",title);
   for (i=0; i<maxrow_ascii; ++i)
   {
      for (j=0; j<(maxcol_ascii+shift1_ascii+shift2_ascii); ++j)
         fprintf(fp,"%c",point[i][j]);
      fprintf(fp,"\n");
   }

   /*  set xlabels */
   for (i=0; i<(shift_ascii-1); ++i) fprintf(fp," ");
   fprintf(fp,"|");
   for (i=shift_ascii+1; i<(shift_ascii+(maxcol_ascii)/2); ++i) fprintf(fp," ");
   fprintf(fp,"|");
   for (i=(shift_ascii+(maxcol_ascii)/2+1); i<(maxcol_ascii+shift_ascii); ++i) fprintf(fp," ");
   fprintf(fp,"|\n");
   fprintf(fp,"      %10.3e                                 %10.3e                                 %10.3e\n",xmin_ascii,0.5*(xmin_ascii+xmax_ascii),xmax_ascii);
   fprintf(fp,"\n");
   for (i=0; i<(shift_ascii+(maxcol_ascii-strlen(xlabel))/2); ++i) fprintf(fp," ");
   fprintf(fp,"%s\n",xlabel);

}











/**************** Definitions of Cubes and Tetrahedrons *****************
*                                                                      *
*                                                                      *
*     (011)------------(111)                    ( 3 )------------( 7 ) *
*       +                +                        +                +   *
*      /.               /|                       /.               /|   *
*     / .              / |                      / .              / |   *
*    /  .             /  |                     /  .             /  |   *
*   /   .            /   |                    /   .            /   |   *
* (001)------------(101) |      <====>      ( 1 )------------( 5 ) |   *
*   |   .            |   |                    |   .            |   |   *
*   | (010)..........|.(110)                  | ( 2 )..........|.( 6 ) *
*   |   .            |   /                    |   .            |   /   *
*   |  .             |  /                     |  .             |  /    *
*   | .              | /                      | .              | /     *
*   |.               |/                       |.               |/      *
*   +                +                        +                +       *
* (000)------------(100)                    ( 0 )------------( 4 )     *
*                                                                      *
*                                                                      *
* Algorithm to find diagaonals                                         *
*                                                                      *
*  Given a cube vertice d1                                             *
*  then d2 = d1^(111) = d1^7                                           *
*                                                                      *
*   Where the cOR bit operator "^" is defined as follows:              *
*      0^0 = 0                                                         *
*      1^1 = 0                                                         *
*      1^0 = 1                                                         *
*      0^1 = 1                                                         *
*                                                                      *
* The four possible cube diagonals are                                 *
*     (000) --- (111)                              (0, 7)              *
*     (001) --- (110)           <====>  2-tuple    (1, 6)              *
*     (010) --- (101)                   rep.       (2, 5)              *
*     (011) --- (100)                              (3, 4)              *
*                                                                      *
* Given a 2-tuple (d1,d2) that defines the diagonal of the cube,       *
* six tetrahedrons are defined, e.g.                                   *
*                                                                      *
*                      (111)                                           *
*                     .  / .                                           *
*                   .   /  .                                           *
*                 .    /   .                                           *
*                .    /   .                                            *
*              .     /    .                                            *
*             .    (101)  .                                            *
*           .     /  |   .        <====> 4-tuple (0, 7, 4, 5)          *
*          .    /    |   .               rep.                          *
*        .    /      |   .                                             *
*       .   /        |  .                                              *
*     .   /          |  .                                              *
*    .  /            |  .                                              *
*  .  /              | .                                               *
* (000)------------(100)                                               *
*                                                                      *
*                                                                      *
* Algorithm to find the six tetradedrons                               *
*                                                                      *
*  Given the diagonals vertices d1 and d2 such that d2=d1^7, the six   * 
*  tetradedrons (six 4-tuples) can be found using the following        *
*  algorithm:                                                          *
*                                                                      *
*   shift(0) = (001) = 1                                               *
*   shift(1) = (010) = 2                                               *
*   shift(2) = (100) = 4                                               *
*   tcount = 0                                                         *
*   For i=0,2                                                          *
*   For j=0,2                                                          *
*     c1 = d1^shift(i)                                                 *
*     c2 = c1^shift(j)                                                 *
*     If (c1 != d1) and (c1 != d2) and (c2!=d1) and (c2!=d2) Then      *
*       tetra(tcount) = (d1,d2,c1,c2)                                  *
*       tcount = tcount + 1                                            *
*     End If                                                           *
*   End For                                                            *
*   End For                                                            *
*                                                                      *
**************** Definitions of Cubes and Tetrahedrons *****************/


double vib_Nstates_Tetra(double e,double *ee)
{
   /* local variables */
   double ds;
   double e1,e2,e4;
   double e21,e31,e41,e32,e42,e43;

   if ((ee[0]<=e)&&(e<ee[1]))
   {
      e1  = e-ee[0];
      e21 = ee[1] - ee[0];
      e31 = ee[2] - ee[0];
      e41 = ee[3] - ee[0];
      ds = e1*e1*e1/(e21*e31*e41);
   }
   else if ((ee[1]<=e)&&(e<ee[2]))
   {
      e2  = e-ee[1];
      e21 = ee[1] - ee[0];
      e31 = ee[2] - ee[0];
      e41 = ee[3] - ee[0];
      e32 = ee[2] - ee[1];
      e42 = ee[3] - ee[1];
      ds = (e21*e21+3.00*e21*e2+3.00*e2*e2-(e31+e42)*e2*e2*e2/(e32*e42))/(e31*e41);
   }
   else if ((ee[2]<=e)&&(e<ee[3]))
   {
      e4  = ee[3]-e;
      e41 = ee[3] - ee[0];
      e42 = ee[3] - ee[1];
      e43 = ee[3] - ee[2];
      ds = 1.00 - e4*e4*e4/(e41*e42*e43);
   }
   else if (e>=ee[3])
   {
      ds = 1.00;
   }
   else
   {
      ds = 0.0;
   }

   return ds;
}

double vib_Nstates_Cube(double e, int *itetra, double *ecube)
{
   /* local variables */
   int i,j,k;
   double ds,etetra[4],swap;

   /* sum over 6 tetrahedrons */
   ds = 0.00;
   for (k=0; k<6; ++k)
   {
      etetra[0] = ecube[itetra[0+4*k]];
      etetra[1] = ecube[itetra[1+4*k]];
      etetra[2] = ecube[itetra[2+4*k]];
      etetra[3] = ecube[itetra[3+4*k]];

      /* bubble sort */
      for (j=0; j<3; ++j)
      for (i=j+1; i<4; ++i)
         if (etetra[j]>etetra[i])
         {
            swap      = etetra[i];
            etetra[i] = etetra[j];
            etetra[j] = swap;
         }
      ds = ds + vib_Nstates_Tetra(e,etetra);
   }
   return ds;
}


double vib_Dstates_Tetra(double e, double *ee)
{
   double ds;
   double e1,e2,e4;
   double e21,e31,e41,e32,e42,e43;

   if ((ee[0]<=e)&&(e<ee[1]))
   {
     e1  = e-ee[1];
     e21 = ee[1] - ee[0];
     e31 = ee[2] - ee[0];
     e41 = ee[3] - ee[0];
     ds = 3.00*e1*e1/(e21*e31*e41);
   }
   else if ((ee[1]<=e)&&(e<ee[2]))
   {
     e2  = e-ee[1];
     e21 = ee[1] - ee[0];
     e31 = ee[2] - ee[0];
     e41 = ee[3] - ee[0];
     e32 = ee[2] - ee[1];
     e42 = ee[3] - ee[1];
     ds = (3.00*e21+6.00*e2-3.00*(e31+e42)*e2*e2/(e32*e42))/(e31*e41);
   }
   else if ((ee[2]<=e)&&(e<ee[3]))
   {
     e4  = ee[3]-e;
     e41 = ee[3] - ee[0];
     e42 = ee[3] - ee[1];
     e43 = ee[3] - ee[2];
     ds = 3.0*e4*e4/(e41*e42*e43);
   }
   else
   {
     ds = 0.0;
   }

   return ds;
}

double vib_Dstates_Cube(double e, int *itetra, double *ecube)
{
   int i,j,k;
   double ds,etetra[4],swap;


   /* sum over 6 tetrahedrons */
   ds = 0.0;
   for (k=0; k<6; ++k)
   {
      etetra[0] = ecube[itetra[0+4*k]];
      etetra[1] = ecube[itetra[1+4*k]];
      etetra[2] = ecube[itetra[2+4*k]];
      etetra[3] = ecube[itetra[3+4*k]];

      /* bubble sort */
      for (j=0; j<3; ++j)
      for (i=j+1; i<4; ++i)
      {
         if (etetra[j]>etetra[i])
         {
            swap      = etetra[i];
            etetra[i] = etetra[j];
            etetra[j] = swap;
         }
      }
      ds = ds + vib_Dstates_Tetra(e,etetra);
   }
   return ds;
}





/*********************************************
 *                                           *
 *             vib_dos_generate              *
 *                                           *
 *********************************************/

void vib_dos_generate(int idx, int idy, int idz, double *eigs,
                      int neigs, int npoints, 
                      double emin, double emax,
                      double *l_unitg, double *efn)
{
   int dosgrid[3],indx;
   int i,j,k,ii,jj,kk,ncubes,ntetra,count;
   int ishft,jshft,kshft;
   int k1_d[4],k2_d[4],k3_d[4],k1_dd[4],k2_dd[4],k3_dd[4];
   int id,d1[4],d2[4];
   int itetra[24];
   double  VT,VG;
   double  B[9],unitg[9],e,ecube[8],f,g,de;
   double  k1,k2,k3,kx,ky,kz,kkx,kky,kkz,r,rmax;

   dosgrid[0] = idx;
   dosgrid[1] = idy;
   dosgrid[2] = idz;

   for (i=0; i<9; ++i) B[i] = l_unitg[i];

   /* volume of reciprocal unit cell, VG */
   unitg[0] = B[4]*B[8] - B[5]*B[7];
   unitg[1] = B[5]*B[6] - B[3]*B[8];
   unitg[2] = B[3]*B[7] - B[4]*B[6];

   unitg[3] = B[7]*B[2] - B[8]*B[1];
   unitg[4] = B[8]*B[0] - B[6]*B[2];
   unitg[5] = B[6]*B[1] - B[7]*B[0];

   unitg[6] = B[1]*B[4] - B[2]*B[4];
   unitg[7] = B[2]*B[3] - B[0]*B[5];
   unitg[8] = B[0]*B[5] - B[1]*B[3];

   VG = B[0]*unitg[0] + B[1]*unitg[1] + B[2]*unitg[2];
      
   ncubes = dosgrid[0]*dosgrid[1]*dosgrid[2];
   ntetra = ncubes*6;
   VT = VG/((double) ntetra);

   /* find shortest diagonal */

   /* (000) ---- (111) */
   k1_d[0] = 0;
   k2_d[0] = 0;
   k3_d[0] = 0;
   k1_dd[0] = 1;
   k2_dd[0] = 1;
   k3_dd[0] = 1;
   d1[0] = 0;
   d2[0] = 7;

   /* (001) ---- (110) */
   k1_d[1] = 1;
   k2_d[1] = 0;
   k3_d[1] = 0;
   k1_dd[1] = 0;
   k2_dd[1] = 1;
   k3_dd[1] = 1;
   d1[1] = 1;
   d2[1] = 6;

   /* (010) ---- (101) */
   k1_d[2] = 0;
   k2_d[2] = 1;
   k3_d[2] = 0;
   k1_dd[2] = 1;
   k2_dd[2] = 0;
   k3_dd[2] = 1;
   d1[2] = 2;
   d2[2] = 5;

   /* (011) ---- (100) */
   k1_d[3] = 1;
   k2_d[3] = 1;
   k3_d[3] = 0;
   k1_dd[3] = 0;
   k2_dd[3] = 0;
   k3_dd[3] = 1;
   d1[3] = 3;
   d2[3] = 4;

   id = 0;
   rmax = 9.99e9;
   for (i=0; i<4; ++i)
   {
      kx = k1_d[i]*B[0] + k2_d[i]*B[3] + k3_d[i]*B[6];
      ky = k1_d[i]*B[1] + k2_d[i]*B[4] + k3_d[i]*B[7];
      kz = k1_d[i]*B[2] + k2_d[i]*B[5] + k3_d[i]*B[8];

      kkx = k1_dd[i]*B[0] + k2_dd[i]*B[3] + k3_dd[i]*B[6];
      kky = k1_dd[i]*B[1] + k2_dd[i]*B[4] + k3_dd[i]*B[7];
      kkz = k1_dd[i]*B[2] + k2_dd[i]*B[5] + k3_dd[i]*B[8];
      r = (kx-kkx)*(kx-kkx) + (ky-kky)*(ky-kky) + (kz-kkz)*(kz-kkz);
      if (r<rmax) 
      {
        rmax = r;
        id = i;
      }
   }


   /* define six tetradrons - clunky but don't know defn of cOR in fortran */
   if (id==0)
   {
      itetra[0] = 0;
      itetra[1] = 7;
      itetra[2] = 1;
      itetra[3] = 3;

      itetra[4] = 0;
      itetra[5] = 7;
      itetra[6] = 1;
      itetra[7] = 5;

      itetra[8] = 0;
      itetra[9] = 7;
      itetra[10] = 2;
      itetra[11] = 3;

      itetra[12] = 0;
      itetra[13] = 7;
      itetra[14] = 2;
      itetra[15] = 6;

      itetra[16] = 0;
      itetra[17] = 7;
      itetra[18] = 4;
      itetra[19] = 5;

      itetra[20] = 0;
      itetra[21] = 7;
      itetra[22] = 4;
      itetra[23] = 6;
   }
   else if (id==1)
   {
      itetra[0] = 1;
      itetra[1] = 6;
      itetra[2] = 0;
      itetra[3] = 2;

      itetra[4] = 1;
      itetra[5] = 6;
      itetra[6] = 0;
      itetra[7] = 4;

      itetra[8] = 1;
      itetra[9] = 6;
      itetra[10] = 3;
      itetra[11] = 2;

      itetra[12] = 1;
      itetra[13] = 6;
      itetra[14] = 3;
      itetra[15] = 7;

      itetra[16] = 1;
      itetra[17] = 6;
      itetra[18] = 5;
      itetra[19] = 4;

      itetra[20] = 1;
      itetra[21] = 6;
      itetra[22] = 5;
      itetra[23] = 7;
   }
   else if (id==2)
   {
      itetra[0] = 2;
      itetra[1] = 5;
      itetra[2] = 3;
      itetra[3] = 1;

      itetra[4] = 2;
      itetra[5] = 5;
      itetra[6] = 3;
      itetra[7] = 7;

      itetra[8] = 2;
      itetra[9] = 5;
      itetra[10] = 0;
      itetra[11] = 1;

      itetra[12] = 2;
      itetra[13] = 5;
      itetra[14] = 0;
      itetra[15] = 4;

      itetra[16] = 2;
      itetra[17] = 5;
      itetra[18] = 6;
      itetra[19] = 7;

      itetra[20] = 2;
      itetra[21] = 5;
      itetra[22] = 6;
      itetra[23] = 4;
   }
   else if (id==3)
   {
      itetra[0] = 3;
      itetra[1] = 4;
      itetra[2] = 2;
      itetra[3] = 0;

      itetra[4] = 3;
      itetra[5] = 4;
      itetra[6] = 2;
      itetra[7] = 6;

      itetra[8] = 3;
      itetra[9] = 4;
      itetra[10] = 1;
      itetra[11] = 0;

      itetra[12] = 3;
      itetra[13] = 4;
      itetra[14] = 1;
      itetra[15] = 5;

      itetra[16] = 3;
      itetra[17] = 4;
      itetra[18] = 7;
      itetra[19] = 6;

      itetra[20] = 3;
      itetra[21] = 4;
      itetra[22] = 7;
      itetra[23] = 5;
   }

   de = (emax-emin)/((double) (npoints-1));
   for (k=0; k<npoints; ++k)
   {
      e = emin + (k-1)*de;

      f = 0.0;
      g = 0.0;
      for (kk=0; kk<dosgrid[2]; ++kk)
      for (jj=0; jj<dosgrid[1]; ++jj)
      for (ii=0; ii<dosgrid[0]; ++ii)
      {
         ishft = ii+1;
         jshft = jj+1;
         kshft = kk+1;
         if (ishft>=dosgrid[0]) ishft=0;
         if (jshft>=dosgrid[1]) jshft=0;
         if (kshft>=dosgrid[2]) kshft=0;
         for (i=0; i<neigs; ++i)
         {
            //ecube[0] = eigs[ii,       jj,    kk, i]; // (000)
            indx = ii + jj*idx + kk*idx*idy + i*idx*idy*idz;
            ecube[0] = eigs[indx];

            //ecube[1] = eigs[ishft,    jj,    kk, i]; // (001)
            indx = ishft+ jj*idx + kk*idx*idy + i*idx*idy*idz;
            ecube[1] = eigs[indx];

            //ecube[2] = eigs[ii,    jshft,    kk, i]; // (010)
            indx = ii + jshft*idx + kk*idx*idy + i*idx*idy*idz;
            ecube[2] = eigs[indx];

            //ecube[3] = eigs[ishft, jshft,    kk, i]; // (011)
            indx = ishft + jshft*idx + kk*idx*idy + i*idx*idy*idz;
            ecube[3] = eigs[indx];

            //ecube[4] = eigs[ii,       jj, kshft, i]; // (100)
            indx = ii + jj*idx + kshft*idx*idy + i*idx*idy*idz;
            ecube[4] = eigs[indx];

            //ecube[5] = eigs[ishft,    jj, kshft, i]; // (101)
            indx = ishft + jj*idx + kshft*idx*idy + i*idx*idy*idz;
            ecube[5] = eigs[indx];

            //ecube[6] = eigs[   ii, jshft, kshft, i]; // (110)
            indx = ii + jshft*idx + kshft*idx*idy + i*idx*idy*idz;
            ecube[6] = eigs[indx];

            //ecube[7] = eigs[ishft, jshft, kshft, i]; // (111)
            indx = ishft + jshft*idx + kshft*idx*idy + i*idx*idy*idz;
            ecube[7] = eigs[indx];

            f = f + vib_Dstates_Cube(e,itetra,ecube);
            g = g + vib_Nstates_Cube(e,itetra,ecube);
         }
      }
      f = f*(VT/VG);
      g = g*(VT/VG);

      efn[k]           = e;
      efn[k+1*npoints] = f;
      efn[k+2*npoints] = g;
   }

}




/*********************************************
 *                                           *
 *              unita_to_abc_abg             *
 *                                           *
 *********************************************/

/* determine a,b,c,alpha,beta,gamma */
void unita_to_abc_abg(double *unita, double *lattice)
{
   double pi,a,b,c,alpha,beta,gamma,d2;
   pi = 4.00*atan(1.0);
   a = sqrt(unita[0]*unita[0] + unita[1]*unita[1] + unita[2]*unita[2]);
   b = sqrt(unita[3]*unita[3] + unita[4]*unita[4] + unita[5]*unita[5]);
   c = sqrt(unita[6]*unita[6] + unita[7]*unita[7] + unita[8]*unita[8]);

   d2 = (unita[3]-unita[6])*(unita[3]-unita[6])
      + (unita[4]-unita[7])*(unita[4]-unita[7])
      + (unita[5]-unita[8])*(unita[5]-unita[8]);
   alpha = (b*b + c*c - d2)/(2.0*b*c);
   alpha = acos(alpha)*180.0/pi;

   d2 = (unita[6]-unita[0])*(unita[6]-unita[0])
      + (unita[7]-unita[1])*(unita[7]-unita[1])
      + (unita[8]-unita[2])*(unita[8]-unita[2]);
   beta = (c*c + a*a - d2)/(2.0*c*a);
   beta = acos(beta)*180.00/pi;

   d2 = (unita[0]-unita[3])*(unita[0]-unita[3])
      + (unita[1]-unita[4])*(unita[1]-unita[4])
      + (unita[2]-unita[5])*(unita[2]-unita[5]);
   gamma = (a*a + b*b - d2)/(2.0*a*b);
   gamma = acos(gamma)*180.0/pi;

   lattice[0] = a;
   lattice[1] = b;
   lattice[2] = c;
   lattice[3] = alpha;
   lattice[4] = beta;
   lattice[5] = gamma;
}




/*********************************************
 *                                           *
 *              vib_get_lattice              *
 *                                           *
 *********************************************/
/*  Given the lattice vectors unita, this function calculates
  the reciprocal lattice (unitg), lattice volume (omega), and the lattice (a,b,c,alpha,beta,gamma)

*/
void vib_get_lattice(double *unita, double *unitg, double *omega, double *lattice)
{
   int i;
   double twopi,omega0;

   twopi = 8.0*atan(1.0);

   /* primitive vectors in the reciprocal space */
   unitg[0] = unita[4]*unita[8] - unita[5]*unita[7];
   unitg[1] = unita[5]*unita[6] - unita[3]*unita[8];
   unitg[2] = unita[3]*unita[7] - unita[4]*unita[6];

   unitg[3] = unita[7]*unita[2] - unita[8]*unita[1];
   unitg[4] = unita[8]*unita[0] - unita[6]*unita[2];
   unitg[5] = unita[6]*unita[1] - unita[7]*unita[0];

   unitg[6] = unita[1]*unita[5] - unita[2]*unita[4];
   unitg[7] = unita[2]*unita[3] - unita[0]*unita[5];
   unitg[8] = unita[0]*unita[4] - unita[1]*unita[3];

   /* volume of a unit cell */
   omega0 = unita[0]*unitg[0] + unita[1]*unitg[1] + unita[2]*unitg[2];
   for (i=0; i<9; ++i) unitg[i] *= (twopi/omega0);

   *omega = omega0;
   unita_to_abc_abg(unita,lattice);
}


/*************************************************
 *                                               *
 *              vib_adjustfordynamic             *
 *                                               *
 *************************************************/
void vib_adjustfordynamic(double *unita, int nion, double *rion,
                          int nion3, double *mass, double *vc, double *w1, double *w2,
                          double *hess, double *hessout)
{
   int ii,jj,i,j,xyz,rst,ierr;
   int i1,i2,i3,dcount,izero,ione,three;
   double dx,dy,dz,x,y,z,d2,d2min,fac,zero,one,mone;


   for (jj=0; jj<nion; ++jj)
   {

      /* diagonal term */
      for (xyz=0; xyz<3; ++xyz)
      for (rst=0; rst<3; ++rst)
      {
         i = 3*jj+xyz;
         j = 3*jj+rst;
         hessout[i+j*nion3] = hess[i+j*nion3];
      }


      for (ii=jj+1; ii<nion; ++ii)
      {
         dx = rion[3*jj]   - rion[3*ii];
         dy = rion[3*jj+1] - rion[3*ii+1];
         dz = rion[3*jj+2] - rion[3*ii+2];

         d2min = 9.99e99;
         for (i3=-1; i3<2; ++i3)
         for (i2=-1; i2<2; ++i2)
         for (i1=-1; i1<2; ++i1)
         {
            x = dx + unita[0]*i1 + unita[3]*i2 + unita[6]*i3;
            y = dy + unita[1]*i1 + unita[4]*i2 + unita[7]*i3;
            z = dz + unita[2]*i1 + unita[5]*i2 + unita[8]*i3;
            d2 = x*x + y*y + z*z;
            if (d2<d2min) d2min = d2;
         }


         dcount = 0;
         for (i3=-1; i3<2; ++i3)
         for (i2=-1; i2<2; ++i2)
         for (i1=-1; i1<2; ++i1)
         {
            x = dx + unita[0]*i1 + unita[3]*i2 + unita[6]*i3;
            y = dy + unita[1]*i1 + unita[4]*i2 + unita[7]*i3;
            z = dz + unita[2]*i1 + unita[5]*i2 + unita[8]*i3;
            d2 = x*x + y*y + z*z;
            if (fabs(d2-d2min)<1.0e-4) dcount = dcount + 1;
         }
            
         fac = 1.0/((double) dcount);
         for (xyz=0; xyz<3; ++xyz)
         for (rst=0; rst<3; ++rst)
         {
            i = 3*ii+xyz;
            j = 3*jj+rst;
            hessout[i+j*nion3] = fac*hess[i+j*nion3];
            hessout[j+i*nion3] = fac*hess[j+i*nion3];
         }
      }
   }

   /* remove translations - hessout = (I-vc*vc')*hessout*(I-vc*vc') */
   zero = 0.0;
   izero = 0;
   ione  = 1;
   ii = 3*nion3;
   dcopy_(&ii,&zero,&izero,vc,&ione);
   x = 1.0/sqrt( ((double) nion) );

   /* unit translation vector for x-, y-, and z-directions */
   for (i=0; i<3; ++i)
   for (ii=0; ii<nion; ++ii)
      vc[(3*ii+i) + i*nion3] = x;
      
   ii = nion3*nion3;
   dcopy_(&ii,&zero,&izero,w1,&ione);
   for (i=0; i<nion3; ++i)
      w1[i+i*nion3] = 1.0;
      
   one  = 1.0;
   mone = -1.0;
   three = 3;
   dgemm_("N","T",&nion3,&nion3,&three,&mone,vc,&nion3,vc,&nion3,&one,w1,&nion3);
   dgemm_("N","N",&nion3,&nion3,&nion3,&one,w1,&nion3,hessout,&nion3,&zero,w2,&nion3);
   dgemm_("N","N",&nion3,&nion3,&nion3,&one,w2,&nion3,w1,&nion3,&zero,hessout,&nion3);

   /* divide by sqrt of masses */
   for (jj=0; jj<nion; ++jj)
   for (ii=0; ii<nion; ++ii)
   {
      fac = 1.0/sqrt(mass[ii]*mass[jj]);
      //fac = fac/1822.890;
      fac = fac;
      for (xyz=0; xyz<3; ++xyz)
      for (rst=0; rst<3; ++rst)
      {
         i = 3*ii+xyz;
         j = 3*jj+rst;
         hessout[i+j*nion3] = hessout[i+j*nion3]*fac;
      }
   }

}



/*************************************************
 *                                               *
 *                 vib_gendynamicmatrix          *
 *                                               *
 *************************************************/

void vib_gendynamicmatrix(double *unita,double *q, int nion, double *rion,
                          int nion3, double *hess, double *dmat, double *wp,
                          double *work,int lwork,double *rwork)
{
   /* local variables */
   int     ii,jj,i,j,xyz,rst,ierr;
   int     i1,i2,i3,dcount;
   double  dx,dy,dz,x,y,z,d2,d2min,qrmin,fac;
   double cfac[2];
      
   for (jj=0; jj<nion; ++jj)
   {

      /* diagonal term */
      for (xyz=0; xyz<3; ++xyz)
      for (rst=0; rst<3; ++rst)
      {
         i = 3*jj+xyz;
         j = 3*jj+rst;
         dmat[0 + i*2 + j*2*nion3] = hess[i+j*nion3];
         dmat[1 + i*2 + j*2*nion3] = 0.0;
      }

      for (ii=jj+1; ii<nion; ++ii)
      {
         dx = rion[3*jj]   - rion[3*ii];
         dy = rion[3*jj+1] - rion[3*ii+1];
         dz = rion[3*jj+2] - rion[3*ii+2];

         d2min=9.99e12;
         for (i3=-1; i3<2; ++i3)
         for (i2=-1; i2<2; ++i2)
         for (i1=-1; i1<2; ++i1)
         {
            x = dx + unita[0]*i1 + unita[3]*i2 + unita[6]*i3;
            y = dy + unita[1]*i1 + unita[4]*i2 + unita[7]*i3;
            z = dz + unita[2]*i1 + unita[5]*i2 + unita[8]*i3;
            d2 = x*x + y*y + z*z;
            if (d2<d2min)
            {
               d2min = d2;
               qrmin = q[0]*x + q[1]*y + q[2]*z;
            }
         }

         cfac[0] = cos(qrmin);
         cfac[1] = sin(qrmin);
      
         for (xyz=0; xyz<3; ++xyz)
         for (rst=0; rst<3; ++rst)
         {
            i = 3*ii+xyz;
            j = 3*jj+rst;

            //dmat(i,j) = cfac        *hess(i,j)
            dmat[0 + i*2 + j*2*nion3] = cfac[0]*hess[i+j*nion3];
            dmat[1 + i*2 + j*2*nion3] = cfac[1]*hess[i+j*nion3];

            //dmat(j,i) = dconjg(cfac)*hess(j,i)
            dmat[0 + j*2 + i*2*nion3] =  cfac[0]*hess[j+i*nion3];
            dmat[1 + j*2 + i*2*nion3] = -cfac[1]*hess[j+i*nion3];
         }
      }
   }

   ii = 2*nion3*nion3;
   x = 1.0e6;
   jj = 1;
   dscal_(&ii,&x,dmat,&jj);
   zheev_("N","U",&nion3,dmat,&nion3,wp,work,&lwork,rwork,&ierr);
   x = 1.0e-6;
   dscal_(&nion3,&x,wp,&jj);

}




/*************************************************
 *                                               *
 *                vib_vdos_thermo                *
 *                                               *
 *************************************************/

/* conversions and constants */
#define AUKCAL 	627.5093314e0
#define clight 	2.998e+10
#define hplank 	6.626e-27
#define kgas 	1.3807e-16
#define Rgas   	(1.9863e0/1000.00/AUKCAL)
#define autocm 	219474.6313705e0
#define autoTHz 6579.683920729e0

void vib_vdos_thermo(FILE *fp, int nion, int npoints, double *efn, double scalfreq, double threshfreq, int ntemp, double *temps)
{
   /* local variables */
   int k,it;
   double temp,RT,RT2,RT1;
   double ezero,ezero1,ezero2;
   double ethermal,ethermal1,ethermal2;
   double fhelmholtz,fhelmholtz1,fhelmholtz2;
   double Svib,Svib1,Svib2;
   double Cv_vib,Cv_vib1,Cv_vib2;
   double sa,sa1,sa2,sb,sb1,sb2,de,g,xdum,hbarw;
   double nmin,nmax,gmin,gmax,emin,emax,xx,yy,zz;

   /* ascii plot of the density of states */
   fprintf(fp,"\n\n\n\n");
   fprintf(fp,"     g(w) and N(w) Used For Calculating Thermodynamic Functions\n");
   fprintf(fp,"     ----------------------------------------------------------\n");
   emin =  9999999.99e0;
   gmin =  9999999.99e0;
   nmin =  9999999.99e0;
   emax = -9999999.99e0;
   gmax = -9999999.99e0;
   nmax = -9999999.99e0;
   for (k=0; k<npoints; ++k)
   {
      xx = efn[k];
      yy = efn[k+npoints];
      zz = efn[k+2*npoints];
      if (xx<emin) emin=xx;
      if (yy<gmin) gmin=yy;
      if (zz<nmin) nmin=zz;
      if (xx>emax) emax=xx;
      if (yy>gmax) gmax=yy;
      if (zz>nmax) nmax=zz;
   }

   fprintf(fp,"\n");
   fprintf(fp,"      Density of States Details\n");
   fprintf(fp,"        - grid size npoints         = %10d\n",npoints);
   fprintf(fp,"        - minimum frequency w       = %10.1lf cm-1\n",emin*autocm);
   fprintf(fp,"        - maximum frequency w       = %10.1lf cm-1\n",emax*autocm);
   fprintf(fp,"        - minimum g(w)              = %10.3le states/cm-1\n",gmin*autocm);
   fprintf(fp,"        - maximum g(w)              = %10.3le states/cm-1\n",gmax*autocm);
   fprintf(fp,"        - mininum N(w)              = %10.1lf states\n",nmin);
   fprintf(fp,"        - maxinum N(w)              = %10.1lf states\n",nmax);
   fprintf(fp,"\n\n");

   ascii_plotter(fp,npoints,1,efn,&efn[npoints], "^", 
                 "g(w) - Vibrational Density of States","w (cm-1)",
                 "g(w) (states/cm-1)",emin,emax,gmin,gmax,1);

   ascii_plotter(fp,npoints,1,efn,&efn[npoints], "*", 
                 "N(w) - Vibrational Density of States","w (cm-1)",
                 "N(w) (states)",emin,emax,nmin,nmax,1);



   /*  Compute the Thermodynamic Functions */
   fprintf(fp,"\n\n");
   fprintf(fp,"     Thermodynamic Functions from Vibrational Density of States\n");
   fprintf(fp,"     ----------------------------------------------------------\n");
   for (it=0; it<ntemp; ++it)
   {
      temp = temps[it];
      RT = Rgas*temp;
      RT2 = 1.00/(2.00*RT);
      RT1 = 1.00/(1.00*RT);

      /**********************************************************************
       **** Write out thermal Corrections to Energies and entropies      ****
       **** frequencies are assumed to be in units of a.u.               ****
       **** Formulas taken from Herzberg                                 ****
       **********************************************************************/
      ezero      = 0.00;
      ezero1     = 0.00;
      ezero2     = 0.00;
      ethermal   = 0.00;
      ethermal1  = 0.00;
      ethermal2  = 0.00;
      fhelmholtz = 0.00;
      fhelmholtz1= 0.00;
      fhelmholtz2= 0.00;
      sa         = 0.00;
      sa1        = 0.00;
      sa2        = 0.00;
      sb         = 0.00;
      sb1        = 0.00;
      sb2        = 0.00;
      Cv_vib     = 0.00;
      Cv_vib1    = 0.00;
      Cv_vib2    = 0.00;
      de = efn[1]-efn[0];
      for (k=1; k<(npoints-1); ++k)
      {
         hbarw = scalfreq*efn[k];
         g     = 0.0*(efn[k+1+2*npoints]-efn[k-1+2*npoints])/de;  //*** central difference derivative ****
         if (temp>0.00)
         {
               xdum = exp(-hbarw*RT1);
         }
         else
         {
            xdum = 0.00;
         }
         
         xdum = xdum/(1.00-xdum);
         ezero      = ezero      + g*hbarw*(0.50);
         ethermal   = ethermal   + g*hbarw*(0.50+xdum);
         fhelmholtz = fhelmholtz + g*RT*log(2.00*sinh(hbarw*RT2));

         sa     = sa     + g*hbarw*xdum;
         sb     = sb     + g*log(1.00-exp(-hbarw*RT1));
         Cv_vib = Cv_vib + g*exp(hbarw*RT1)*(hbarw*RT1*xdum)*(hbarw*RT1*xdum);
         if (hbarw<threshfreq)
         {
            ezero1      = ezero1   + g*hbarw*(0.50);
            ethermal1   = ethermal1+ g*hbarw*(0.50+xdum);
            fhelmholtz1=fhelmholtz1+g*RT*log(2.00*sinh(hbarw*RT2));
            sa1    = sa1    + g*hbarw*xdum;
            sb1    = sb1    + g*log(1.00-exp(-hbarw*RT1));
            Cv_vib1= Cv_vib1+ g*exp(hbarw*RT1)*(hbarw*RT1*xdum)*(hbarw*RT1*xdum);
         }
         else
         {
            ezero2      = ezero2   + g*hbarw*(0.50);
            ethermal2   = ethermal2+ g*hbarw*(0.50+xdum);
            fhelmholtz2=fhelmholtz2+g*RT*log(2.00*sinh(hbarw*RT2));
            sa2    = sa2    + g*hbarw*xdum;
            sb2    = sb2    + g*log(1.00-exp(-hbarw*RT1));
            Cv_vib2= Cv_vib2+ g*exp(hbarw*RT1)*(hbarw*RT1*xdum)*(hbarw*RT1*xdum);
         }
 
      }

      ezero      = ezero*de;
      ezero1     = ezero1*de;
      ezero2     = ezero2*de;
      ethermal   = ethermal*de;
      ethermal1  = ethermal1*de;
      ethermal2  = ethermal2*de;
      fhelmholtz = fhelmholtz*de;
      fhelmholtz1= fhelmholtz1*de;
      fhelmholtz2= fhelmholtz2*de;
      if (temp>0.00)
      {
         Svib = sa*de/temp  - Rgas*sb*de;
         Svib1= sa1*de/temp - Rgas*sb1*de;
         Svib2= sa2*de/temp - Rgas*sb2*de;
      }
      else
      {
         Svib = 0.00;
         Svib1= 0.00;
         Svib2= 0.00;
      }
      
      Cv_vib = Rgas*Cv_vib*de;
      Cv_vib1= Rgas*Cv_vib1*de;
      Cv_vib2= Rgas*Cv_vib2*de;

      fprintf(fp,"      Temperature                       = %8.2lf K (RT=%13.6le au)\n",temp,RT);
      fprintf(fp,"      frequency scaling parameter       = %8.4lf\n",scalfreq);
      fprintf(fp,"      frequency dividing parameter      = %8.1lf\n",threshfreq*autocm);

      fprintf(fp,"       Helmholtz Free Energy             = %8.3lf kcal/mol (%10.6lf au)\n",fhelmholtz*AUKCAL,fhelmholtz);
      fprintf(fp,"       - (less than    %8.1lf cm-1)    = %8.3lf kcal/mol (%10.6lf au)\n",threshfreq*autocm,fhelmholtz1*AUKCAL,fhelmholtz1);
      fprintf(fp,"       - (greater than %8.1lf cm-1)    = %8.3lf kcal/mol (%10.6lf au)\n",threshfreq*autocm,fhelmholtz2*AUKCAL,fhelmholtz2);

      fprintf(fp,"       Zero-Point correction to Energy   = %8.3lf kcal/mol (%10.6lf au)\n",ezero*AUKCAL,ezero);
      fprintf(fp,"       - (less than    %8.1lf cm-1)    = %8.3lf kcal/mol (%10.6lf au)\n",threshfreq*autocm,ezero1*AUKCAL,ezero1);
      fprintf(fp,"       - (greater than %8.1lf cm-1)    = %8.3lf kcal/mol (%10.6lf au)\n",threshfreq*autocm,ezero2*AUKCAL,ezero2);

      fprintf(fp,"       Thermal correction to Energy      = %8.3lf kcal/mol (%10.6lf au)\n",ethermal*AUKCAL,ethermal);
      fprintf(fp,"       - (less than    %8.1lf cm-1)    = %8.3lf kcal/mol (%10.6lf au)\n",threshfreq*autocm,ethermal1*AUKCAL,ethermal1);
      fprintf(fp,"       - (greater than %8.1lf cm-1)    = %8.3lf kcal/mol (%10.6lf au)\n",threshfreq*autocm,ethermal2*AUKCAL,ethermal2);


      fprintf(fp,"       Entropy                           = %8.3lf cal/mol-K \n",Svib*AUKCAL*1000.00);
      fprintf(fp,"       - (less than    %8.1lf cm-1)    = %8.3lf cal/mol-K\n",threshfreq*autocm,Svib1*AUKCAL*1000.00);
      fprintf(fp,"       - (greater than %8.1lf cm-1)    = %8.3lf cal/mol-K\n",threshfreq*autocm,Svib2*AUKCAL*1000.00);

      fprintf(fp,"       Cv (constant volume heat capacity) = %8.3lf cal/mol-K \n",Cv_vib*AUKCAL*1000.00);
      fprintf(fp,"       - (less than    %8.1lf cm-1)    = %8.3lf cal/mol-K\n",threshfreq*autocm,Cv_vib1*AUKCAL*1000.00);
      fprintf(fp,"       - (greater than %8.1lf cm-1)    = %8.3lf cal/mol-K\n",threshfreq*autocm,Cv_vib2*AUKCAL*1000.00);
      fprintf(fp,"\n");

   }

}








/*********************************************
 *                                           *
 *                vib_phonon                 *
 *                                           *
 *********************************************/
/*
   Entry - fp: filepointer
           nion: number of atoms
           mass[nion]: mass of atoms in atomic units
           unita[9]: lattice vectors in atomic units
           rion[3*nion]: position of atoms in atomic units
           hess[3*nion*3*nion]: hessian in atomic units
   Exit -
*/
void vib_phonon(FILE *fp, int nion, double *mass, double *unita, double *rion, double *hess, int *dosgrid)
{
   int    nion3,lwork,i,j,icount,i0,i1,i2,indx,npoints;
   double ks[3],kq[3],xxx,yyy,zzz,emin,emax;
   double omega,unitg[9],lattice[6],temps[4];

   double *eigdos,*dmat,*hessadjust,*wp,*rwork,*work,*w2;  /*temp arrays*/
   double *efn_dos;

   nion3 = 3*nion;

   vib_get_lattice(unita, unitg, &omega, lattice);
   fprintf(fp,"\n\n");
   fprintf(fp,"                Phonon Spectra Analysis\n");
   fprintf(fp,"\n");
   fprintf(fp,"    number of atoms = %5d\n",nion);
   fprintf(fp,"    number of bands = %5d\n",nion3);
   fprintf(fp,"\n");
   fprintf(fp,"     Lattice Parameters\n");
   fprintf(fp,"     ------------------\n\n");
   fprintf(fp,"     lattice vectors in a.u.\n");
   fprintf(fp,"     a1=<%8.3lf %8.3lf %8.3lf>\n",unita[0],unita[1],unita[2]);
   fprintf(fp,"     a2=<%8.3lf %8.3lf %8.3lf>\n",unita[3],unita[4],unita[5]);
   fprintf(fp,"     a3=<%8.3lf %8.3lf %8.3lf>\n",unita[6],unita[7],unita[8]);
   fprintf(fp,"     a=      %8.3lf b=    %8.3lf c=     %8.3lf\n",lattice[0],lattice[1],lattice[2]);
   fprintf(fp,"     alpha=  %8.3lf beta= %8.3lf gamma= %8.3lf\n",lattice[3],lattice[4],lattice[5]);
   fprintf(fp,"     omega=  %8.3lf\n\n",omega);
   fprintf(fp,"     reciprocal lattice vectors in a.u.\n");
   fprintf(fp,"     b1=<%8.3lf %8.3lf %8.3lf>\n",unitg[0],unitg[1],unitg[2]);
   fprintf(fp,"     b2=<%8.3lf %8.3lf %8.3lf>\n",unitg[3],unitg[4],unitg[5]);
   fprintf(fp,"     b3=<%8.3lf %8.3lf %8.3lf>\n",unitg[6],unitg[7],unitg[8]);

   /* allocate temporary arrays */
   npoints = 1001;
   efn_dos     = (double *) malloc(3*npoints*sizeof(double));
   eigdos      = (double *) malloc(dosgrid[0]*dosgrid[1]*dosgrid[2]*nion3*sizeof(double));
   dmat        = (double *) malloc(nion3*nion3*sizeof(double)*2);
   hessadjust  = (double *) malloc(nion3*nion3*sizeof(double));
   wp          = (double *) malloc(nion3*sizeof(double));
   rwork       = (double *) malloc(3*nion3*sizeof(double));
   lwork       = nion3*nion3;
   if (lwork<(3*nion3)) lwork = 3*nion;
   work = (double *) malloc(lwork*sizeof(double)*2);

   /* perform Eckart projections and divide by sqrt of masses */
   w2 = (double *) malloc(nion3*nion3*sizeof(double));
   vib_adjustfordynamic(unita,nion,rion,nion3,mass,rwork,work,w2,hess,hessadjust);
   free(w2);



   /* Density of States */
   icount = 0;
   xxx = 1.00/(1.00*dosgrid[0]);
   yyy = 1.00/(1.00*dosgrid[1]);
   zzz = 1.00/(1.00*dosgrid[2]);
   for (i2=0; i2<dosgrid[2]; ++i2)
   for (i1=0; i1<dosgrid[1]; ++i1)
   for (i0=0; i0<dosgrid[0]; ++i0)
   {
      ks[0] = i0*xxx;
      ks[1] = i1*yyy;
      ks[2] = i2*zzz;
      kq[0] = unitg[0]*ks[0]+unitg[3]*ks[1]+unitg[6]*ks[2];
      kq[1] = unitg[1]*ks[0]+unitg[4]*ks[1]+unitg[7]*ks[2];
      kq[2] = unitg[2]*ks[0]+unitg[5]*ks[1]+unitg[8]*ks[2];

      vib_gendynamicmatrix(unita,kq,nion,rion,nion3,hessadjust,
                           dmat,wp,work,lwork,rwork);

      for (i=0; i<nion3; ++i)
      {
        indx = icount + i*dosgrid[0]*dosgrid[1]*dosgrid[2];
        eigdos[indx] = sqrt(fabs(wp[i]));
      }

      fprintf(fp,"\n%d out of %d\n",icount+1,dosgrid[0]*dosgrid[1]*dosgrid[2]);
      fprintf(fp,"kvec=  <%lf %lf %lf>\n",kq[0],kq[1],kq[2]);
      fprintf(fp,"ksvec= <%lf %lf %lf>\n",ks[0],ks[1],ks[2]);
      for (i=0; i<nion3; ++i)
         fprintf(fp,"     wp[%4d]= %10.3le %10.3lf cm-1\n",i,sqrt(fabs(wp[i])),sqrt(fabs(wp[i]))*autocm);

      ++icount;

   }

   free(work);
   free(rwork);
   free(wp);
   free(hessadjust);
   free(dmat);


   /******************** DOS plotting **************************/
   emin = 0.0;
   emax = -99999.09;
   for (i=0; i<(nion3*dosgrid[0]*dosgrid[1]*dosgrid[2]); ++i)
      if (eigdos[i]>emax) emax=eigdos[i];
    
   vib_dos_generate(dosgrid[0],dosgrid[1],dosgrid[2],
                    eigdos,nion3,npoints,emin,emax,unitg,efn_dos);

   //for (i=0; i<npoints; ++i)
   //   printf("%le  %le %le\n",efn_dos[i],efn_dos[i+npoints],efn_dos[i+2*npoints]);

   temps[0] = 298.15;
   vib_vdos_thermo(fp,nion,npoints,efn_dos,1.0,100.00/autocm,1,temps);

   free(eigdos);
   free(efn_dos);



}


int main()
{
   int    nion,nion3,id,ii,jj,dosgrid[3];
   char   atom[2],line[500];
   double x,y,z,m;
   double rion[3*100],mass[100],hess[3*100*3*100],unita[9];
   FILE *fp;

   printf("Hello Phonon\n");

   fp = fopen("geom.dat","r");
   nion = 0;
   while (fscanf(fp,"%s %d %lf %lf %lf %lf",atom,&id,&x,&y,&z,&m)!=EOF) 
   {
      rion[3*nion]   = x;
      rion[3*nion+1] = y;
      rion[3*nion+2] = z;
      mass[nion] = m*1822.89;
      ++nion;
   }
   fclose(fp);
   nion3 = 3*nion;

   fp = fopen("hess.dat","r");
   while (fscanf(fp,"%d",&ii)!=EOF)
   {
      for (jj=0; jj<nion3; ++jj)
      {
         fscanf(fp,"%lf",&x);
         hess[(ii-1)+jj*nion3] = x;
      }
   }
   fclose(fp);

   dosgrid[0] = 10;
   dosgrid[1] = 10;
   dosgrid[2] = 10;
   unita[0] = 20.0; unita[1] =  0.0; unita[2] =  0.0;
   unita[3] =  0.0; unita[4] = 20.0; unita[5] =  0.0;
   unita[6] =  0.0; unita[7] =  0.0; unita[8] = 20.0;
   vib_phonon(stdout,nion,mass,unita,rion,hess,dosgrid);
   
}

