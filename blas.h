#ifndef _BLAS_H_
#define _BLAS_H_

extern  void dcopy_(int *, double *, int *, double *, int *);
extern  double ddot_(int *, double *, int *, double *, int *);
extern  void daxpy_(int *, double *, double *, int *, double *, int *);
extern  void dscal_(int *, double *, double *, int *);
extern  void dgemm_(char *, char *, int *, int *, int *,
                       double *, 
                       double *, int *,
                       double *, int *,
                       double *,
                       double *, int *);

//extern "C" void eigen_(int *, int *, double *, double *, double *, int *);

extern  void dsyev_(char *, char *, int *,
                       double *, int *,
                       double *,
                       double *, int *, int*);
extern  int  idamax_(int *, double *, int *);

extern  void zheev_(char *, char *, int *,
                       double *, int *,
                       double *,
                       double *, int *, double *, int*);

#endif


